import spotipy
from spotipy.oauth2 import SpotifyOAuth
import json
from Song import Song

'''
GET the liked songs from the Spotify account
Make a playlist of the .json
    - Custom playlist name 
'''

CLIENT_ID =  "" #input("[State] CLIENT_ID: ")
CLIENT_SECRET = "" #input("[State] CLIENT_SECRET: ")


#scopes = "playlist-modify-public user-read-recently-played playlist-modify-private"
scopes = "user-library-read"
sp = spotipy.Spotify(auth_manager=SpotifyOAuth(client_id=CLIENT_ID,
                                               client_secret=CLIENT_SECRET,
                                               redirect_uri="http://localhost:8888/callback/", scope=scopes))

songs = []
def GetTracks():
    print("[State] Getting tracks...")
    limit = 20

    r = sp.current_user_saved_tracks(limit=1)
    total = r['total']

    print("[State] Total: ", total)
    rounds = round(total / limit) if total > limit else 1

    '''if total > limit:
        rounds = round(total / limit)'''

    print("[State] Rounds: ", rounds)

    for i in range(rounds):
        r = sp.current_user_saved_tracks(limit=limit, offset=limit*i)
        print(f"[State] *** Round: %i *** [items: %i]" % (i + 1, len(r["items"])))
        for j in r['items']:
            songs.append(Song(len(songs)+1,j['track']['name'], j['track']['uri']))
            #print(j['track']['uri'])


def CurrentSongs(s, max, offset):
    r = []
    print(f"[State] MAX: %i OFFSET: %i" % (max, offset))
    for i in range(offset, len(s)):
        if i > offset+max: break
        r.append(s[i])

    print(r)
    return r

def CreatePlaylist(p):
    '''
        New account data
    '''
    CLIENT_ID = ""
    CLIENT_SECRET = ""
    account_id = ""
    scopes = "playlist-modify-public user-read-recently-played playlist-modify-private"
    sp = spotipy.Spotify(auth_manager=SpotifyOAuth(client_id=CLIENT_ID,
                                                   client_secret=CLIENT_SECRET,
                                                   redirect_uri="http://localhost:8888/callback/", scope=scopes))


    res = sp.user_playlist_create(user=account_id, name=p, public=True)
    playlist_id = res['id']
    print(f"[State] Playlist created with name of %s [id=%s]." % (p, playlist_id))

    songs2 = []
    for i in range(len(songs)):
        songs2.append(songs[len(songs)-1-i].uri)


    MAX_SONGS_PER_REQUEST = 99

    rounds = 1
    if len(songs2) > MAX_SONGS_PER_REQUEST:
        rounds = round(len(songs2) / MAX_SONGS_PER_REQUEST)

    print(rounds)

    for i in range(rounds):

        add_response = sp.playlist_add_items(playlist_id=playlist_id, items=CurrentSongs(songs2, MAX_SONGS_PER_REQUEST, i*MAX_SONGS_PER_REQUEST))

    print(f"[State] Songs added to %s." % (p))
    print(add_response)

def SaveToLikedLibrary():
    CLIENT_ID = ""
    CLIENT_SECRET = ""
    account_id = ""
    scopes = "user-library-modify"
    sp = spotipy.Spotify(auth_manager=SpotifyOAuth(client_id=CLIENT_ID,
                                                   client_secret=CLIENT_SECRET,
                                                   redirect_uri="http://localhost:8888/callback/", scope=scopes))
    songs2 = []
    for i in range(len(songs)):
        songs2.append(songs[len(songs) - 1 - i].uri)

    MAX_SONGS_PER_REQUEST = 49

    rounds = 1
    if len(songs2) > MAX_SONGS_PER_REQUEST:
        rounds = round(len(songs2) / MAX_SONGS_PER_REQUEST)

    print(rounds)

    for i in range(rounds):
        sp.current_user_saved_tracks_add(tracks=CurrentSongs(songs2, MAX_SONGS_PER_REQUEST, i*MAX_SONGS_PER_REQUEST))




GetTracks()

t = int(input("Save on the new account as: [0] playlist || [1] Liked Songs: "))

if t == 0:
    print("Saving as playlist...")

    playlist_name = input("New playlist name? (Default: Synced Songs):")
    if playlist_name == "": playlist_name = "Synced Songs"


    CreatePlaylist(playlist_name)


else:
    print("Saving to the Liked Songs library...")

    SaveToLikedLibrary()



print("Bye! Songs: ", len(songs))