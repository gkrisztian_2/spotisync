class Song:
    def __init__(self, id, name, uri):
        self.name = name
        self.uri = uri
        self.id = id
        print(f"[Song] %i. %s [%s]" % (self.id, self.name, self.uri))